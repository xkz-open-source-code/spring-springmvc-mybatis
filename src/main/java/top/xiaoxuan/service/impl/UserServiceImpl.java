package top.xiaoxuan.service.impl;

import top.xiaoxuan.dao.UserDao;
import top.xiaoxuan.entity.User;
import top.xiaoxuan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Service  //为了包扫描的时候这个Service被扫描到
public class UserServiceImpl implements UserService {

    final UserDao userDao;

    @Autowired
    private UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public User get(Serializable id) {
        return userDao.get(id);
    }

    public List<User> find(Map<?, ?> map) {
        return userDao.find(map);
    }

    public void insert(User user) {
        userDao.insert(user);
    }

    public void update(User user) {
        userDao.update(user);
    }

    public void deleteById(Serializable id) {
        userDao.deleteById(id);
    }

    public void delete(Serializable[] ids) {
        userDao.delete(ids);
    }
}
