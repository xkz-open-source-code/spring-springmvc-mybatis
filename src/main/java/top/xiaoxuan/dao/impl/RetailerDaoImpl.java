package top.xiaoxuan.dao.impl;

import top.xiaoxuan.dao.RetailerDao;
import top.xiaoxuan.entity.Retailer;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository //为了包扫描的时候这个Dao被扫描到
public class RetailerDaoImpl extends BaseDaoImpl<Retailer> implements RetailerDao {
    public RetailerDaoImpl() {
        //设置命名空间  
        super.setNs("top.xiaoxuan.mapper.RetailerMapper");
    }

    //实现接口自己的方法定义
    public int count(Map<?, ?> map) {
        return this.getSqlSession().selectOne(this.getNs() + ".count", map);
    }
}
